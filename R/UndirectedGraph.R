#' Transform to undirected graph
#'
#' Take a matrix giving the edgelist for a directed multigraph, and return
#' an igraph object of the associated undirected graph, where there is an
#' edge a<->b if either a->b or b->a at least once in the original graph.
#'
#' @param g A matrix giving the edgelist of a directed multigraph, where
#' nodes are potentially index by real numbers.
#' @return h The edgelist of an undirected graph where there is an edge
#' \eqn{a\iff b} if either \eqn{a \rightarrow b} or
#' \eqn{b \rightarrow a} at least once in \eqn{g}.
#' @export
UndirectedGraph <- function(g) {
    Z.edgelist <- unique(rbind(g, g[, c(2, 1)]))
    return (Z.edgelist)
}
