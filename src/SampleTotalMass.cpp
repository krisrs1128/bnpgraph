#include <Rcpp.h>
#include <math.h>

using namespace Rcpp;

double SampleU(double gamma_var, double w1, double w2, 
	       double w3, double lambda, double alpha,
	       double xi, double psi) {
  double V = R::runif(0.0, 1.0);
  double W_prime = R::runif(0, 1);
  double U = 0;
  if (gamma_var >= 1) {
    if (V < w1 / (w1 + w2)) {
      U = std::abs(rnorm(1)[0]) / sqrt(gamma_var);
    } else {
      U = M_PI * (1 - pow(W_prime, 2));
    }
  } else {
    if (V < w3 / (w3 + w2)) {
      U = M_PI * W_prime;
    } else {
      U = M_PI * (1 - pow(W_prime, 2));
    }
  }
  return U;
}

double IndicatorProd1(double U, double gamma_var, double xi) {
  if (U >= 0 & gamma_var >= 1) {
    return xi * exp(-.5 * gamma_var * pow(U, 2));
  } else {
    return 0.0;
  }
}

double IndicatorProd2(double U, double psi) {
  if(0 < U < M_PI) {
    return psi / sqrt(M_PI - U);
  } else {
    return 0;
  }
}

double IndicatorProd3(double U, double gamma_var, double xi) {
  if(0 <= U) {
    if (U <= M_PI) {
      if (gamma_var < 1) {
	return xi;
      } else {
	return 0;
      }
    } else {
      return 0;
    }
  } else {
    return 0;
  }
}

double Indicator4(double X, double m) {
  if (X < m) {
    return 1.0;
  } else {
    return 0.0;
  }
}

double Indicator5(double X, double m, double delta) {
  if (X > m + delta) {
    return 1.0;
  } else {
    return 0.0;
  }
}

double A(double x, double alpha) {
  // Zolatarev's function
  return (sin(x) / (pow(sin(alpha * x), alpha) * pow(sin(1 - alpha) * x, 1 - alpha)));
}

double B(double x, double alpha) {
  double result;
  if (x == 0) {
    return 1.0 / (pow(alpha, alpha) * pow(1 - alpha, 1 - alpha));
  } else {
    return pow(A(x, alpha), alpha - 1);
  }
}

//' Sample from an exponentially tilted positive stable law
//' 
//' This implements the algorithm on page 18:18 of Devroye's
//' "Random Variate Generation for Exponentially and Polynomially
//' Stable Distributions," which is necessary for sampling from
//' the normalized generalized gamma process. Specifically, this
//' samples a variable \eqn{S_{\alpha, \lambda}} with Laplace transform
//' \deqn{E[e^{-\mu S_{\alpha, \lambda}}] = e^{\lambda^{\alpha} - (\mu + \lambda)^{\alpha}}}
//' 
//' @param alpha Stable Law parameter in the Laplace Transform.
//' @param lambda Tilting parameter appearing in the Laplace Transform.
//' @return A single draw from this distribution.
// [[Rcpp::export]]
double SAlphaLambda(double alpha, double lambda) {
  double result;
  while (true) {
    double gamma_var = pow(lambda, alpha) * alpha * (1 - alpha);
    double xi = (2 + sqrt(M_PI / 2.0)) * (1.0 / M_PI) * sqrt(2 * gamma_var) + 1;
    double psi = (1.0 / M_PI) * (exp(- gamma_var * pow(M_PI, 2) / 8.0)) * 
      (2 + sqrt(M_PI / 2.0)) * sqrt(gamma_var * M_PI);
    double w1 = xi * sqrt(M_PI / (2.0 * gamma_var));
    double w2 = 2 * psi * sqrt(M_PI);
    double w3 = xi * M_PI;
    double b = (1 - alpha) / alpha;

    double U;
    double z;
    
    while(true) {
      U = SampleU(gamma_var, w1, w2, w3, lambda, alpha, xi, psi);
      double W = R::runif(0, 1);
      double zeta = sqrt(B(U, alpha) / B(0, alpha));
      double phi = pow(sqrt(gamma_var) + alpha * zeta, 1.0 / alpha);
      z = phi / (phi - pow(sqrt(gamma_var), 1.0 / alpha));
      
      // evaluate g**(U) / g*(U)
      double rho = (M_PI * exp(-pow(lambda, alpha) * (1 - pow(zeta, -2))) *  
		    (IndicatorProd1(U, gamma_var, xi) +  
		     IndicatorProd2(U, psi) + IndicatorProd3(U, gamma_var, xi))) /  
	((1 + sqrt(M_PI / 2.0)) * sqrt(gamma_var) / zeta + z);
      if (U < M_PI & W * rho < 1) break;
    }

    /* Generate X with density proportional to g(x, U) */

    // setup constants
    double a = A(U, alpha);
    double m = pow(b * lambda / a, alpha);
    double delta = sqrt(m * alpha / a);
    double a1 = delta * sqrt(M_PI / 2);
    double a2 = delta;
    double a3 = z / a;
    double s = a1 + a2 + a3;

    double V_prime = R::runif(0, 1);
    double X = 0;
    double N_prime;
    double E_prime = rexp(1)[0];
    if (V_prime < a1 / s) {
      N_prime = rnorm(1)[0];
      X = m - delta * std::abs(N_prime);
    } else if (V_prime < a2 / s) {
      X = R::runif(m, m + delta);
    } else {
      X = m + delta + E_prime * a3;
    }

    double E = rexp(1)[0];
    
    if (X >= 0 & a * (X - m) + lambda * (pow(X, -b) - pow(m, -b)) - 
	(1.0 / 2) * pow(N_prime, 2) * Indicator4(X, m) - E_prime * 
	Indicator5(X, m, delta) <= E) {
      result = pow(X, -b);
      break;
    }
  }
  return result;
}

//' Sample the total mass of an NGGP
//'
//' Draw a sample of the total mass of a CRM underlying measure
//' \deqn{\frac{a}{\Gamma(1 - \sigma)} \xi^{-\sigma - 1} e^{-\tau \xi} d\xi \lambda_{\alpha}(d\theta)}
//' where \eqn{\lambda_{\alpha}(d\theta)} is the Lebesgue measure on \eqn{[0, \alpha]}.
//'
//' @param a A shape parameter in the form above.
//' Determines rate of decay in the degree distribution. Determines
//' rate of decay in the degree distribution.
//' @param tau A shape parameter in the form above. Determines
//' rate of decay in the degree distribution.
//' @param sigma The discount parameter in the form above. Determines
//' the slope of the degree distribution, and hence the sparsity of the
//' resulting networks.
//' @param alpha The cap for the uniform base measure. Determines
//' the number of nodes in the resulting network.
//' 
// [[Rcpp::export]]
double SampleNGGPMass(double a, double tau, double sigma, double alpha) {
  double T = 0;
  if (sigma == 0) {
    T = rgamma(1, alpha * a, 1) [0];
  } else {
    double theta = a / sigma;
    // Use laplace transform of iid sum (with extra term for noninteger alpha)
    for(int i = 0; i < floor(alpha); i++) {
      T += pow(theta, (1 / sigma)) * SAlphaLambda(sigma, tau * pow(theta, 1 / sigma));
    }
    if (alpha - floor(alpha) != 0) {
      T += pow(a * (alpha - floor(alpha)) / sigma, 1 / sigma) *
        SAlphaLambda(sigma, tau * pow(a / sigma * (alpha - floor(alpha)), 1 / sigma));
    }
  }
  return (T);
}
