#include <Rcpp.h>

using namespace Rcpp;

//' Sample stick breaking
//'
//' Sample a single point from a stick breaking process.
//'
//' @param alpha We draw a GEM(alpha) distribution.
//' @param T The truncation level
// [[Rcpp::export]]
NumericVector StickBreak(double alpha, int T) {
  NumericVector result(T);
  double one_minus_v_prod = 1.0;
  for(int i = 0; i < T; i++) {
    double V = rbeta(1, 1, alpha)[0];
    result[i] = (one_minus_v_prod) * V;
    one_minus_v_prod *= (1 - V);
  }
  return result;
}

//' Sample stick breaking
//' 
//' Sample n points from a stick breaking process.
//' 
//' @param n Number of samples to return.
//' @param alpha We draw a GEM(alpha) distribution.
//' @param T The truncation level
// [[Rcpp::export]]
NumericMatrix SampleSB(int n, double alpha, int T) {
  NumericMatrix result(n, T);
  for(int i = 0; i < n; i++) {
    result(i, _) = StickBreak(alpha, T);
  }
  return result;
}
