#include <Rcpp.h>

using namespace Rcpp;

//' Prune a network
//'
//' This provides a mechanism for pruning a network based on the
//' affinities of nodes, which is the basis for the sparse netework
//' model with communities.
//'
//' @param D An \eqn{n \times 2} matrix parameterizing the directed 
//' multigraph before networks have been pruned.
//' @param link_probs An \eqn{n \times n} matrix supplying the link
//' probabilities between two nodes, which presumably depends on 
//' their community membership.
//' @param D_ix An integer matrix indexing which row of the link
//' probabilities matrix corresponds to which edge of D. That is,
//' the D[D_ix[i, 0]] row of link_probs gives the probability that
//' the node in row i of D links to any of the other unique nodes
//' in the graph (where the unique labels in D_ix give the
//'  corresponding) column.
//' 
//' @return D_prune The directed multigraph after pruning.
// [[Rcpp::export]]
NumericMatrix MixedMembershipPrune(NumericMatrix D, NumericMatrix link_probs,
				   IntegerMatrix D_ix) {
  std::vector<int> keep_link;
  for(int i = 0; i < D.nrow(); i++) {
    double p_link = link_probs(D_ix(i, 0), D_ix(i, 1));
    if(rbinom(1, 1, p_link)[0] == 1) {
      keep_link.push_back(i);
    }
  }

  NumericMatrix D_prune(keep_link.size(), 2);
  for(int i = 0; i < keep_link.size(); i++) {
    D_prune(i, _) = D(keep_link[i], _);
  }

  return D_prune;
}
