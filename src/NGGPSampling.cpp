#include <Rcpp.h>

using namespace Rcpp;

// An urn process is a vector of clusters
struct cluster {
  double location;
  int count;
  cluster(double location, int count) : location(location), count(count) {
  }
};

struct graph {
  std::set<double> node_pair;
  int edge_count;
};

int NumElements(std::vector<cluster> urn_process) {
  int n = 0;
  for(int i = 0; i < urn_process.size(); i++) {
    n += urn_process[i].count;
  }
  return n;
}

NumericVector ClusterProbabilities(std::vector<cluster> urn_process, 
				   int r, double a, double U, 
				   double tau, double sigma) {
  // First entry indexes for new cluster
  int k = urn_process.size(); 
  NumericVector cluster_probs(k + 1);
  for(int i = 0; i < k; i++) {
    cluster_probs(i) = (urn_process[i].count - sigma) / 
      (a * pow(U + tau, sigma) + r - sigma * k);
  }
  cluster_probs(k) = (a * pow(U + tau, sigma)) / 
    (a * pow(U + tau, sigma) + r - sigma * k);
  return cluster_probs;
}

int NextLocation(NumericVector cluster_probs) {
  double p = R::runif(0.0, 1.0);
  double running_sum = 0;
  for(int i = 0; i < cluster_probs.size(); i++) {
    running_sum += cluster_probs(i);
    if(p < running_sum) {
      return i;
    }
  }
}

//' Sample from an NGGP, keeping track of cluster sizes.
//' 
//' Instead of returning the sequence of outcomes from
//' an urn process, sometimes it is useful to know the
//' number of elements in each cluster, along with the 
//' cluster location. This loses information of the
//' ordering, however.
//'
//' @param n The number of samples to draw from the process.
//' @param a A shape parameter in the form above.
//' Determines rate of decay in the degree distribution. Determines
//' rate of decay in the degree distribution.
//' @param U The auxiliary variable appearing in the EPPF above.
//' @param tau A shape parameter in the form above. Determines
//' rate of decay in the degree distribution.
//' @param sigma The discount parameter in the form above. Determines
//' the slope of the degree distribution, and hence the sparsity of the
//' resulting networks.
//' @param alpha The cap for the uniform base measure. Determines
//' the number of nodes in the resulting network.
//'
//' @return urn_counts A vector giving the location and frequency
//' of the sample.
//' @seealso \code{NGGPSample}
// [[Rcpp::export]]
NumericMatrix NGGPClusterSizes(int n, double a, double U, 
				  double tau, double sigma,
				  double alpha) {
  /* Assume base measure is Unif(0, alpha) */
  std::vector<cluster> urn_process;
  urn_process.push_back(cluster(R::runif(0.0, alpha), 1));
  int k = 1;
  for(int i = 1; i < n; i++) {
    NumericVector cluster_probs = ClusterProbabilities(urn_process, i, a, U, tau, sigma);
    int next_cluster_ix = NextLocation(cluster_probs);
    if(next_cluster_ix == k) {
      urn_process.push_back(cluster(R::runif(0.0, alpha), 1));
      k++;
    } else {
      urn_process[next_cluster_ix].count++;
    }
  } 
  NumericMatrix urn_counts(k, 2);
  for(int i = 0; i < k; i++) {
    urn_counts(i, 0) = urn_process[i].location;
    urn_counts(i, 1) = (double)urn_process[i].count;
  }
  return(urn_counts);
}

//' Sample from an NGGP urn process
//'
//' This uses the parameterization of Favaro and Teh "MCMC
//' for Normalized Random Measure Mixture Models" to draw
//' samples from a NGGP. This is slightly different from that
//' of Fox and Carron. In particular, let \eqn{U ~ Gamma(n, T)}, where
//' \eqn{T} is the total mass of an NGGP. Then we apply the update
//' 2.15 of Favaro and Teh,
//' \deqn{\theta_{n + 1} \mid U, \theta_{1}, \dots, \theta_{n} \sim  \frac{a(U + \tau)^{\sigma}}{a(U + \tau)^{\sigma} + n - \sigma |K|} \lambda_{\alpha} + \sum_{k =1 }^{K} \frac{n_{k} - \sigma}{a(U + \tau)^{\sigma} + n - \sigma K} \delta_{\theta_{k^{\ast}}},}
//' where \eqn{\theta_{k}^{\ast}} indexes the unique values of \eqn{\theta}
//' and there are \eqn{K} such unique values.
//' 
//' @param n The number of samples to draw from the process.
//' @param a A shape parameter in the form above.
//' Determines rate of decay in the degree distribution. Determines
//' rate of decay in the degree distribution.
//' @param U The auxiliary variable appearing in the EPPF above.
//' @param tau A shape parameter in the form above. Determines
//' rate of decay in the degree distribution.
//' @param sigma The discount parameter in the form above. Determines
//' the slope of the degree distribution, and hence the sparsity of the
//' resulting networks.
//' @param alpha The cap for the uniform base measure. Determines
//' the number of nodes in the resulting network.
//'
//' @return A vector giving the \eqn{n} samples from the NGGP.
//'
//' @examples
//' NGGPSample(10, 1.0, 1.0, 1.0, 0.5, 10)
// [[Rcpp::export]]
NumericVector NGGPSample(int n, double a, double U, 
			 double tau, double sigma, 
			 double alpha) {
  /* Assume base measure is Unif(0, alpha) */
  std::vector<double> urn_loc;
  std::vector<cluster> urn_process;
  double u = R::runif(0.0, alpha);
  urn_process.push_back(cluster(u, 1));
  urn_loc.push_back(u);
  int k = 1;
  for(int i = 1; i < n; i++) {
    NumericVector cluster_probs = ClusterProbabilities(urn_process, i, a, U, tau, sigma);
    int next_cluster_ix = NextLocation(cluster_probs);
    if(next_cluster_ix == k) {
      double u = R::runif(0.0, alpha);
      urn_process.push_back(cluster(u, 1));
      k++;
      urn_loc.push_back(u);
    } else {
      urn_process[next_cluster_ix].count = urn_process[next_cluster_ix].count + 1;
      // first element is "new cluster"
      urn_loc.push_back(urn_process[next_cluster_ix].location);
    }
  }
  NumericVector urn_loc_R((int) urn_loc.size());
  for(int i = 0; i < urn_loc.size(); i++) {
    urn_loc_R(i) = urn_loc[i];
  }
  return (urn_loc_R);
}
